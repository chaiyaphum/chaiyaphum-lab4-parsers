/*
 * FeedSeaecher.java (DOM)
 * This programe get the URL of a feed such as http://www.blognone.com/atom.xml.
 * Next it will show title and link of RSS feed 
 * At the same time you can search the feed title with the given keyword.
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

package coe.kku.webservice;

import java.io.*;
import java.net.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

@WebServlet(name = "FeedSeaecher", urlPatterns = {"/Chaiyaphum-FeedSearcher"})
public class FeedSeaecher extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("<html><body><table	border='1'><tr><th>Title</th><th>Link</th></tr>");
        try {

            //Use JAXP to find parser
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            // Turn namespace support
            factory.setNamespaceAware(true);
            DocumentBuilder parser = factory.newDocumentBuilder();
            // Get url and keyword from index.jsp
            String urlLoc = request.getParameter("url");
            String keywordLoc = request.getParameter("keyword");
            URL url = new URL(urlLoc);

            //Read the Entire document to memory
            Document doc = parser.parse(url.openStream());


            NodeList items = doc.getElementsByTagName("item");
            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);

                //if keyword and title is matching program will show title and url   
                if ((getElemVal(item, "title").toLowerCase()).contains(keywordLoc.toLowerCase())) {
                    out.println("<tr><td>" + getElemVal(item, "title") + "</td>");
                    String link = getElemVal(item, "link");
                    out.println("<td><a	href='" + link + "'>" + link + "</a></td><tr>");
                }
            }
            out.print("</table></body></html>");
        } catch (Exception e) {
            System.out.print(e);
        } finally {
            out.close();
        }
    }

    // Method getElemVal will get element value of chlid node
    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}