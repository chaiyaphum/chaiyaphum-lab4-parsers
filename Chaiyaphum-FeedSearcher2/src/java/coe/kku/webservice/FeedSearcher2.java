/*
 * FeedSeaecher2.java (StAX)
 * This programe get the URL of a feed such as http://www.blognone.com/atom.xml.
 * Next it will show title and link of RSS feed 
 * At the same time you can search the feed title with the given keyword.
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

package coe.kku.webservice;

import java.io.*;
import java.net.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedSearcher2", urlPatterns = {"/Chaiyaphum-FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader reader;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        String titleData = null;
        String linkData = null;
        String elementName = null;

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        // Get url and keyword from index.jsp
        String inputURL = request.getParameter("url");
        String inputKeyword = request.getParameter("keyword");

        try {
            URL u = new URL(inputURL);
            InputStream in = u.openStream();

            // Create object XMLInputFactory for XMLEventReader
            XMLInputFactory factory = XMLInputFactory.newInstance();

            //Create object createXMLEventReader for read XML file from URL
            reader = factory.createXMLEventReader(in);
            out.print(
                    "<html><body><table	border = '1'><tr ><th>Title</th><th>Link</th ></tr>");

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                // If this event is part of start element
                if (event.getEventType() == XMLEvent.START_ELEMENT) {
                    StartElement startE = event.asStartElement();
                    elementName = startE.getName().getLocalPart();
                    // If found start element name "item" set itemFound is true
                    if (elementName.equals("item")) {
                        itemFound = true;
                    }
                    // If found start element name "link" set linkFound is true
                    if (itemFound && elementName.equals("link")) {
                        linkFound = true;
                    }
                    // If found start element name "title" set titleFound is true
                    if (itemFound && elementName.equals("title")) {
                        titleFound = true;
                    }
                }

                // If this event is part of end element
                if (event.getEventType() == XMLEvent.END_ELEMENT) {
                    EndElement endE = event.asEndElement();
                    elementName = endE.getName().getLocalPart();
                    // If found end element name "item" set itemFound is false
                    if (elementName.equals("item")) {
                        itemFound = false;
                    }
                    // If found end element name "link" set linkFound is false
                    if (itemFound && elementName.equals("link")) {
                        linkFound = false;
                    }
                    // If found end element name "title" set titleFound is false
                    if (itemFound && elementName.equals("title")) {
                        titleFound = false;
                    }
                }
                
                // If this event is part of characters
                if (event.getEventType() == XMLEvent.CHARACTERS) {
                    Characters chars = event.asCharacters();
                    
                    if (titleFound) {
                        titleData = chars.getData();
                    }
                    if (linkFound) {
                        linkData = chars.getData();
                        
                        //if keyword and title is matching program will show title and url 
                        if (titleData.toLowerCase().contains(inputKeyword.toLowerCase())) {
                            out.print("<tr><td>");
                            out.print(titleData);
                            out.print("</td>");
                            out.print("<td>");
                            // Make link as format <a herf="url">content</a>
                            out.print("<a href='" + linkData + "' >" + linkData + "</a>");
                            out.print("</td></tr>");
                        }
                    }
                }
            }
            // end while

            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}