<%-- 
    Document   : index
    Created on : 22 พ.ย. 2555, 22:12:21
    Author     : Chaiyaphum Siripanpornchana  533040441-5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JA Simple Feed Writer</title>
    </head>
    <body>
        <h1>A Simple Feed Writer</h1>
        <form action="FeedWriter">
            Title:&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;<input type="text" size="80" maxsize="200" name="title"/>
            <br/><br/>
            Link:&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;<input type="text" size="80" maxsize="200" name="url"/>
            <br/><br/>
            Description: <input type="text" size="80" maxsize="200" name="description"/>
            <br/><br/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
