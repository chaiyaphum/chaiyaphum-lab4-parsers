/*
 * FeedWriterForm.java (DOM)
 * This programe can write those input information into rss feed.
 * The latest information is written as the last item in a feed
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

package coe.kku.webservice;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

@WebServlet(name = "FeedWriter", urlPatterns = {"/Chaiyaphum-FeedWriterForm"})
public class FeedWriter extends HttpServlet {

    Document doc;
    // filePath is a location of feed file
    String filePath = "feed.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Set encoding of request data is UTF-8
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");        
        PrintWriter out = response.getWriter();
        
        // Get title, url and description from index.jsp
        String rssTitle = request.getParameter("title");
        String rssUrl = request.getParameter("url");
        String rssDescription = request.getParameter("description");
        
        try {
            if (file.exists()) {
                // If the feed file exits, the lastest filled information
                // is appended to the existing file
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.parse(file);
                FeedWriter fw = new FeedWriter();
                String feed = fw.updateRss(doc, rssTitle, rssUrl, rssDescription);
                out.println(feed);
            } else {
                
                // If the feed file does not exits, the programe creates
                // a new feed file
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.newDocument();
                FeedWriter fw = new FeedWriter();
                String feed = fw.createRss(doc, rssTitle, rssUrl, rssDescription);
                out.print(feed);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // If the feed file does not exits, the programe creates
    // a new feed file
    public String createRss(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {
        //root element rss
        Element rss = doc.createElement("rss");
           
        //attribute version = '2.0'in element rss
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);
        
        //the child element of rss is channel
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);

        // Element/rss/channel/title
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);
        
        //element /rss/channel/description
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kea University Information News Rss Feed");
        desc.appendChild(descT);

        //element/rss/channel/link
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        //element/rss/channel/lang
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);

        //element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);

        //element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);
        //element/rss/channel/description
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);
        //element/rss/channel/link
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);
        //element/rss/channel/pubDate
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //Transformerfactory instance is used to create Transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    // If the feed file exits, the lastest filled information
    // is appended to the existing file
    public String updateRss(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);

        //element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);
        
        //element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);

        //element/rss/channel/description
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);

        //element/rss/channel/link
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);

        //element/rss/channel/pubDate
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //TransformerFactory instance is used to create transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate file name in the web project
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
    // </editor-fold>
}