/*
 * XMLFileSearcher.java (Stax)
 * This java application that search quotes with the given authors.
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

package coe.kku.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLFileSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException {
        String keywordFilePath = "keyword.txt";
        File quoteFile = new File("qoutes.xml");
        boolean qouteFound = false;
        boolean wordFound = false;
        boolean byFound = false;
        String keyword = null;
        String word = null;
        String by = null;
        String eName = null;
        Scanner scanner = new Scanner(new FileInputStream(keywordFilePath), "UTF-8");

        try {
            while (scanner.hasNextLine()) {
                keyword = scanner.nextLine();
                // Create an output input
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(quoteFile)));

                // Iterate until there is no more data to read
                while (reader.hasNext()) {
                    // Get the next XMLEvent
                    XMLEvent event = reader.nextEvent();
                    
                    // If this part of data is the start tag
                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = true;
                        }
                        if (qouteFound && eName.equals("word")) {
                            wordFound = true;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }

                    // Found end element
                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = false;
                        }
                        if (qouteFound && eName.equals("word")) {
                            wordFound = false;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }

                    // If this part of data is characters section
                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;

                        if(byFound) {
                            by = characters.getData();
                            
                            if(by.toLowerCase().contains(keyword.toLowerCase())) {
                                // 
                                System.out.println(word + " by " + by);
                            }
                        }
                        if(wordFound) {
                            word = characters.getData();
                        }
                    }
                }
            }

        } finally {
            scanner.close();
        }
    }
}