/*
 * XMLFileWriter.java (Stax)
 * This java application that it produces an XML File "quotes.xml"
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

package coe.kku.webservice;

import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XmlWriter2 {

    public static void main(String argv[]) {

        File file = new File("quotes.xml");

        try {

            // Write an output factory
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            // Write an xml stream writer
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));

            // Write XML prologue
            xtw.writeStartDocument("UTF-8", "1.0");

            // Create quotes to root element
            xtw.writeStartElement("quotes");

            xtw.writeStartElement("quote");
            xtw.writeStartElement("word");
            xtw.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement();
            // End element quote
            xtw.writeEndElement();

            xtw.writeStartElement("quote");
            xtw.writeStartElement("word");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement();
            // End elemrnt quote
            xtw.writeEndElement();

            // End document
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();
            
            System.out.println("Create quotes.xml Success!!!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
